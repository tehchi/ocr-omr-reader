# By Babak Safaei
"""Automate the data extaction from survey forms in Python.

Major steps are as followed:

1- Load list of images in grayscale

2- Preprocess the image by running a set of classification methods

3- Identify answerboxes and run OMR

4- Gather the results from the two previous parts and extract them into a .csv files

Use packages of os, cv2, imutils, pandas, numpy, matplotlib.
"""
# importing the required packages
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import imutils
from imutils.perspective import four_point_transform as f1
import cv2

# mapping rows of sections and answers in form
MAP = {'driver_visibility': range(1, 3), 'vehicle_type': range(4, 9),
       'driver_phone_use': range(10, 13),
       'driver_seatbelt': range(14, 16), 'driver_age': range(17, 20),
       'brake_lights': range(21, 23),
       'passenger_location': range(24, 26), 'lane': range(27, 30),
       'passenger_seatbelt': range(31, 33)}

NUM_RW, NUM_CL = 33, 7  # number of rows and columns in the survey form
UP = 0  # top margin info of the survey form for cutting
DOWN = 0  # down margin info of the survey form for cutting
RIGHT = 70  # right margin info of the survey form for cutting
LEFT = 70  # left margin info of the survey form for cutting
TOPX, TOPY = 496, 79  # starting point of grids in top and left of survey form
STEPX, STEPY = 267, 77  # step size for answer boxes

def bin_blk(image):
    """Build an empty black image or a 2d array of 0s."""
    return np.zeros(image.shape, image.dtype)


def bin_wht(image):
    """Build an empty white image or a 2d array of 255s."""
    return 255 * np.ones(image.shape, image.dtype)


def find_cont(image):
    """Return the contours of the image to get the outline of the form."""
    cont = cv2.findContours(image, cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)  # find the contours
    cont = imutils.grab_contours(cont)  # grab the contours
    # sort the contours to select the biggest one
    cont = sorted(cont, key=cv2.contourArea, reverse=True)
    return cont


def pre_process(image):
    """Reduce noise using bluring, contrast brightness adjustments and edge detection."""
    image = cv2.GaussianBlur(
        image, (5, 5), 0)  # to decrease the high frequency noise
    image = cv2.addWeighted(image, 1.5, bin_wht(
        image), 0, 0)  # adjust constrast and brightness
    # use canny for edge and outline detection of the form
    image = cv2.Canny(image, 75, 200)
    return image


def bird_eye(image, min_box_area=6000000):  # 2410*2657=6403370 size of outer box
    """Return a bird eye view and contours of the document."""
    #  crop the image and cut the useless parts
    cropped_image = image[UP:image.shape[0] -
                          DOWN, LEFT:image.shape[1] - RIGHT]
    # preprocess the image
    pre_processed_image = pre_process(cropped_image)
    # find contours of the form to get the outer box or form outline
    cont = find_cont(pre_processed_image)
    # calculate the area of the outline of the form (area of the outer box)
    box_area = cv2.contourArea(cont[0])
    # approximate the contours
    peri = cv2.arcLength(cont[0], True)
    approx = cv2.approxPolyDP(cont[0], .02 * peri, True)
    # check to see if the found contour is for the outer box
    assert not (box_area < min_box_area or len(
        approx) != 4), 'outline not identified'
    # apply four point transform to get the bird eye  top down view of the form
    bird = f1(cropped_image.copy(), approx.reshape(4, 2))
    return bird, cont


def show_img(path):
    """Show or plot the image from the given path."""
    read = plt.imread(path)
    plt.imshow(read)


def draw_img(image):
    """Draw the image at any stage of the program."""
    plt.imshow(image)


def plot_data(x_plot, y_plot, xlabel="x", ylabel="y", color="blue"):
    """Plot the desired data vs eachother in scatter format."""
    plt.scatter(x_plot, y_plot, c=color)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)


def find_cellbox(image, i, j):
    """Find and return the answer cellbox and its surrounding area."""
    y_start = TOPY + i * STEPY
    y_end = TOPY + (i + 1) * STEPY
    x_start = TOPX + j * STEPX
    x_end = TOPX + (j + 1) * STEPX
    find = image[y_start: y_end, x_start: x_end]
    return find


def answer_omr(darks):
    """Return the marked answers to questions."""
    # check to see if the image shape is correct
    assert darks.shape == (
        NUM_RW, NUM_CL), ('Bad image shape: (%d, %d)' % darks.shape)

    answers = {}
    for (q_name, choice_range) in MAP.items():  # loop over the questions and choice boxes
        # get the section of answers for a question in whole form
        darks_section = darks[choice_range, :]
        question_choices = []
        for j in range(NUM_CL):  # loop over the 7 columns of form

            # get one columns of answers for a question in whole form
            darks_section_col = darks_section[:, j]
            # find the answer index with the darkest box
            choice = np.argmax(darks_section_col)
            # save the marked answer box index
            darkness_choice = darks[choice_range[choice], j]
            if darkness_choice > .01:  # if the box is black marked enough
                # append the found answer to the answers list
                question_choices.append(choice + 1)
            else:
                # if nothing was found mark the question as empty
                question_choices.append(0)
            # record the question and its found answer in our dict
            answers[q_name] = question_choices
    return answers


def get_files(path):
    """Return the list of images in the selected path."""
    files = []
    for file in os.listdir(path):  # loop over the list of files in the given path
        if file.endswith(".jpg"):  # see if the files are .jpg images
            # joins the path component with / and file name
            files.append(os.path.join(path, file))
    files = sorted(files)
    return files


def imgs_load(files):
    """Return the an array of gray images from the files list."""
    imgs_array = []
    for file in files:  # loop over the files list to read images
        # read images in a grayscale format
        imgs_array.append(cv2.imread(file, cv2.IMREAD_GRAYSCALE))
    return np.array(imgs_array)


def run_omr(path):
    """Run OMR on the scanned image and return transferred data and answers."""
    files = get_files(path)  # get the list of files in the path
    images = imgs_load(files)  # read images in gray scale and store them
    data_final = pd.DataFrame()  # build the dataframe for final data

    for image in images:  # loop over all the images in the available list

        # get the contour and bird eye view of the form
        image = bird_eye(image)[0]
        image = cv2.addWeighted(image.copy(), 1.5, bin_wht(
            image), 0, 0)  # adjust the contrast and brightness

        # build a zero black array to size of form
        pandora = np.zeros((NUM_RW, NUM_CL), dtype=np.float32)
        for i in range(NUM_RW):
            for j in range(NUM_CL):
                                # record the relative darkness of answer boxes in pandora array
                whiteness = np.sum(find_cellbox(image, i, j)) / (255 * STEPX * STEPY)
                pandora[i, j] = 1-whiteness
        answers = answer_omr(pandora)  # find marked answer boxes
        # record answers to a panda data frame
        data_temp = pd.DataFrame.from_dict(answers)

        # append temporary data to the final data frame
        data_final = pd.concat([data_final, data_temp])
    # add vehicle countsas index to data frame
    data_final.insert(0, 'vehicle_count', np.arange(1, len(files) * NUM_CL+1))
    data_final.columns = ["Number", "Driver Visible", "Type", "Driver Phone Use", "Driver Seatbelt",
                          "Driver Age", "Brake Lights", "Passenger Loc",
                          "Lane", "Passenger Seatbelt"]
    # save the data array as a csv file
    data_final.to_csv('%s/traffic.csv' % path, index=False)
    return data_final, answers
