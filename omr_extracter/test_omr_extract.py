"""This module is supposed to test omr_extract.py module."""

import omr_extract

def test_run_omr():
    """Returns the result of testing the module."""
    assert omr_extract.run_omr('images/')[1] == {'driver_visibility': [1, 2, 0, 0, 0, 0, 0],
                                                 'vehicle_type': [0, 1, 2, 3, 0, 3, 5],
                                                 'driver_phone_use': [0, 2, 3, 1, 2, 3, 1],
                                                 'driver_seatbelt': [0, 0, 0, 2, 1, 1, 1],
                                                 'driver_age': [0, 3, 0, 3, 2, 1, 1],
                                                 'brake_lights': [0, 0, 0, 2, 0, 0, 1],
                                                 'passenger_location': [0, 0, 0, 0, 1, 2, 1],
                                                 'lane': [2, 1, 1, 2, 0, 0, 0],
                                                 'passenger_seatbelt': [0, 0, 0, 2, 1, 0, 0]}
    