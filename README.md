# omr-extracter

This project will be focused on providing an automated approach for extracting the data from safety survey forms into comma separated files for further analysis. This work can be done through optical mark recognition (OMR). I am going to make  more effective traffic survey form for traffic surveys by changing the old ones. The form needs to be scannable after it is filled out by road surveyors. An OMR method will be applied for reading the information from these forms and extracting them. A final accuracy measurement approach is also required to assess the accuracy of the software. For accuracy measurement, information from the forms will be extracted through the software and manual procedure and then will be compared to each other.

Usually, the results of these surveys include hundreds of observations, and the manual approach for entering the data is very time-consuming. The mentioned point emphasizes the requirement of such an automated method in this field. The final software can be used by the MSU transportation research group or other parties needing it.

Numpy, cv2 (OpenCV), matplotlib, pandas, imutils, seaborn are expected to be used. Previous OMR, OCR approaches for other tasks already exist on OpenCv and GitHub, which will be used to build the foundation of the program.


Installation Guidelines:

Please create a conda environment using following commands.

conda env create --prefix ./envs --file environment.yml
conda activate ./envs

Place the files of your scanned images in the images folder. Import the omr_extract.py package and run the following command to get the data in a .csv format. The .csv file will be created in the images folder. Also, the command below will store the results in a data spreadsheet and provide a summary of it.

data = omr_extract.run_omr('images/')[0]

data.describe()